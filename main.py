import sys
import os
import tempfile
from PyQt5.QtWidgets import (
    QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout,
    QLabel, QLineEdit, QPushButton, QTextEdit, QMessageBox, QDialog, QRadioButton
)
from PyQt5.QtGui import QFont, QPalette, QLinearGradient, QColor, QBrush

# Путь к файлу с историей вычислений
storage_path = os.path.join(tempfile.gettempdir(), 'historyComputing.txt')

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle("Сортировка чисел")
        self.setGeometry(100, 100, 600, 450)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.create_widgets()
        self.create_layout()

    def create_widgets(self):
        self.label_numbers = []
        self.lineedit_numbers = []
        for i in range(5):
            self.label_numbers.append(QLabel(f'Число {i+1}:'))
            self.lineedit_numbers.append(QLineEdit())

            self.label_numbers[i].setFont(QFont('Comic Sans MS', 10))
            self.lineedit_numbers[i].setFont(QFont('Verdana', 8))

        self.btn_sort = QPushButton("Сортировать")
        self.btn_sort.setFont(QFont('Lucida Sans Unicode', 9))
        self.btn_sort.clicked.connect(self.sort_numbers)

        self.btn_history = QPushButton("История")
        self.btn_history.setFont(QFont('Lucida Sans Unicode', 9))
        self.btn_history.clicked.connect(self.show_history)

        self.radio_asc = QRadioButton("По возрастанию")
        self.radio_asc.setFont(QFont('Verdana', 10))
        self.radio_asc.setChecked(True)

        self.radio_desc = QRadioButton("По убыванию")
        self.radio_desc.setFont(QFont('Verdana', 10))

    def create_layout(self):
        layout = QVBoxLayout()
        self.central_widget.setLayout(layout)

        for i in range(5):
            row_layout = QHBoxLayout()
            row_layout.addWidget(self.label_numbers[i])
            row_layout.addWidget(self.lineedit_numbers[i])
            layout.addLayout(row_layout)

        button_layout = QHBoxLayout()
        button_layout.addWidget(self.radio_asc)
        button_layout.addWidget(self.radio_desc)
        layout.addLayout(button_layout)

        button_layout2 = QHBoxLayout()
        button_layout2.addWidget(self.btn_sort)
        button_layout2.addWidget(self.btn_history)
        layout.addLayout(button_layout2)


        self.setStyleSheet("""
            QRadioButton:hover {
                background-color: #B8D8BE;
            }
        """)
    def sort_numbers(self):
        try:
            numbers = []
            for lineedit in self.lineedit_numbers:
                number = float(lineedit.text().strip())
                numbers.append(number)

            if self.radio_asc.isChecked():
                sorted_numbers = sorted(numbers)
                sorted_text = '; '.join(map(str, sorted_numbers))
                sort_type = "по возрастанию"
            elif self.radio_desc.isChecked():
                sorted_numbers = sorted(numbers, reverse=True)
                sorted_text = '; '.join(map(str, sorted_numbers))
                sort_type = "по убыванию"

            strRes = 'Отсортированные числа ' + str(sort_type) + ': ' + str(sorted_text)
            QMessageBox.information(self, 'Сортировка чисел', strRes)

            self.save_to_history(strRes)

        except ValueError:
            QMessageBox.warning(self, 'Ошибка', 'Пожалуйста, введите числа корректно.')

        except Exception:
            QMessageBox.warning(self, 'Ошибка', 'Неизвестная ошибка.')

    def save_to_history(self, text):
        if not os.path.exists(storage_path):
            open(storage_path, 'w').close()

        with open(storage_path, 'a') as f:
            f.write(text + '\n')

    def show_history(self):
        if not os.path.exists(storage_path):
            QMessageBox.information(self, 'История', 'Нет истории вычислений.')
            return

        history_dialog = HistoryDialog(self)
        history_dialog.exec_()

    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Выход', 'Вы действительно хотите выйти из приложения?',
                                     QMessageBox.Yes, QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()


class HistoryDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("История вычислений")
        self.setGeometry(200, 200, 1000, 300)

        self.create_widgets()
        self.create_layout()

        self.load_history()

    def create_widgets(self):
        self.textedit_history = QTextEdit()
        self.textedit_history.setFont(QFont('Verdana', 10))
        self.textedit_history.setReadOnly(True)

        self.btn_clear_history = QPushButton("Очистить историю")
        self.btn_clear_history.setFont(QFont('Lucida Sans Unicode', 9))
        self.btn_clear_history.clicked.connect(self.clear_history)

        self.btn_close = QPushButton("Закрыть")
        self.btn_close.setFont(QFont('Lucida Sans Unicode', 9))
        self.btn_close.clicked.connect(self.close)

        # Применение стиля с градиентом к QTextEdit
        gradient = QLinearGradient(0, 0, 0, self.height())
        gradient.setColorAt(0.0, QColor(220, 220, 240))
        gradient.setColorAt(1.0, QColor(240, 240, 240))

        palette = QPalette()
        palette.setBrush(QPalette.Base, gradient)
        self.textedit_history.setPalette(palette)

    def create_layout(self):
        layout = QVBoxLayout()
        self.setLayout(layout)

        layout.addWidget(self.textedit_history)

        button_layout = QHBoxLayout()
        button_layout.addWidget(self.btn_clear_history)
        button_layout.addWidget(self.btn_close)
        layout.addLayout(button_layout)

    def load_history(self):
        if not os.path.exists(storage_path):
            self.textedit_history.setText("Нет истории вычислений.")
            return

        with open(storage_path, 'r') as f:
            history_text = f.read()
            self.textedit_history.setText(history_text)

    def clear_history(self):
        if not os.path.exists(storage_path) or os.path.getsize(storage_path) == 0:
            QMessageBox.warning(self, 'Ошибка', 'История вычислений пуста. Невозможно очистить историю.')
            return

        reply = QMessageBox.question(self, 'Очистить историю', 'Вы действительно хотите очистить историю?',
                                     QMessageBox.Yes, QMessageBox.No)

        if reply == QMessageBox.Yes:
            self.textedit_history.clear()
            open(storage_path, 'w').close()





if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()

    p = QPalette()
    gradient = QLinearGradient(0, 0, 0, 400)
    gradient.setColorAt(0.0, QColor(220, 220, 240))
    gradient.setColorAt(1.0, QColor(240, 200, 200))
    p.setBrush(QPalette.Window, QBrush(gradient))
    window.setPalette(p)

    window.show()
    sys.exit(app.exec_())
