import sys
import os
import tempfile
from PyQt5.QtWidgets import (
    QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout,
    QLabel, QLineEdit, QPushButton, QTextEdit, QMessageBox, QAction,
    QFileDialog, QDialog, QRadioButton
)
from PyQt5.QtCore import Qt

# Путь к файлу с историей вычислений
storage_path = os.path.join(tempfile.gettempdir(), 'historyComputing.txt')

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.setWindowTitle("Сортировка чисел")
        self.setGeometry(100, 100, 400, 250)

        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.create_widgets()
        self.create_layout()

    def create_widgets(self):
        self.label_numbers = []
        self.lineedit_numbers = []
        for i in range(5):
            self.label_numbers.append(QLabel(f'Число {i+1}:'))
            self.lineedit_numbers.append(QLineEdit())

        self.btn_sort = QPushButton("Сортировать")
        self.btn_sort.clicked.connect(self.sort_numbers)

        self.btn_history = QPushButton("История")
        self.btn_history.clicked.connect(self.show_history)

        self.radio_asc = QRadioButton("По возрастанию")
        self.radio_asc.setChecked(True)
        self.radio_desc = QRadioButton("По убыванию")

    def create_layout(self):
        layout = QVBoxLayout()
        self.central_widget.setLayout(layout)

        for i in range(5):
            row_layout = QHBoxLayout()
            row_layout.addWidget(self.label_numbers[i])
            row_layout.addWidget(self.lineedit_numbers[i])
            layout.addLayout(row_layout)

        button_layout = QHBoxLayout()
        button_layout.addWidget(self.radio_asc)
        button_layout.addWidget(self.radio_desc)
        layout.addLayout(button_layout)

        button_layout2 = QHBoxLayout()
        button_layout2.addWidget(self.btn_sort)
        button_layout2.addWidget(self.btn_history)
        layout.addLayout(button_layout2)

    def sort_numbers(self):
        try:
            numbers = []
            for lineedit in self.lineedit_numbers:
                number = float(lineedit.text().strip())
                numbers.append(number)

            if self.radio_asc.isChecked():
                sorted_numbers = sorted(numbers)
                sorted_text = '; '.join(map(str, sorted_numbers))
                sort_type = "по возрастанию"
            elif self.radio_desc.isChecked():
                sorted_numbers = sorted(numbers, reverse=True)
                sorted_text = '; '.join(map(str, sorted_numbers))
                sort_type = "по убыванию"

            QMessageBox.information(self, 'Сортировка чисел', f'Отсортированные числа {sort_type}: {sorted_text}')

            self.save_to_history(sorted_text)

        except ValueError:
            QMessageBox.warning(self, 'Ошибка', 'Пожалуйста, введите числа корректно.')

    def save_to_history(self, text):
        if not os.path.exists(storage_path):
            open(storage_path, 'w').close()

        with open(storage_path, 'a') as f:
            f.write(text + '\n')

    def show_history(self):
        if not os.path.exists(storage_path):
            QMessageBox.information(self, 'История', 'Нет истории вычислений.')
            return

        history_dialog = HistoryDialog(self)
        history_dialog.exec_()

    def closeEvent(self, event):
        reply = QMessageBox.question(self, 'Выход', 'Вы действительно хотите выйти из приложения?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

class HistoryDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("История вычислений")
        self.setGeometry(200, 200, 400, 300)

        self.create_widgets()
        self.create_layout()

        self.load_history()

    def create_widgets(self):
        self.textedit_history = QTextEdit()
        self.textedit_history.setReadOnly(True)

        self.btn_clear_history = QPushButton("Очистить историю")
        self.btn_clear_history.clicked.connect(self.clear_history)

        self.btn_close = QPushButton("Закрыть")
        self.btn_close.clicked.connect(self.close)

    def create_layout(self):
        layout = QVBoxLayout()
        self.setLayout(layout)

        layout.addWidget(self.textedit_history)

        button_layout = QHBoxLayout()
        button_layout.addWidget(self.btn_clear_history)
        button_layout.addWidget(self.btn_close)
        layout.addLayout(button_layout)

    def load_history(self):
        if not os.path.exists(storage_path):
            self.textedit_history.setText("Нет истории вычислений.")
            return

        with open(storage_path, 'r') as f:
            history_text = f.read()
            self.textedit_history.setText(history_text)

    def clear_history(self):
        reply = QMessageBox.question(self, 'Очистить историю', 'Вы действительно хотите очистить историю?',
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)

        if reply == QMessageBox.Yes:
            self.textedit_history.clear()
            open(storage_path, 'w').close()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
